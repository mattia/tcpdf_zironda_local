<?php
require_once('config/tcpdf_config.php');
require_once('tcpdf.php');

class hPDF extends TCPDF {

    public $footer_txt;

    // Load table data from file
    public function LoadData($file) {
        // Read file lines
        $lines = file($file);
        $data = array();
        foreach($lines as $line) {
            $data[] = explode(';', chop($line));
        }
        return $data;
    }

    private function getWidth($array) {
        $size = 0;
        foreach ($array as $value) {
            $txt_size = $this->GetStringWidth(trim($value),'courier', '', 8, $getarray=false)+3;
            if ($txt_size >= $size) $size = $txt_size;
        }
        return $size;
    }

    public function Footer() {
        $cur_y = $this->y;
        $this->SetTextColorArray($this->footer_text_color);
        //set style for cell border
        $line_width = (0.85 / $this->k);
        $this->SetLineStyle(array('width' => $line_width, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => $this->footer_line_color));
        $this->SetFont('courier', '', 7);
        $this->MultiCell('', '', $this->footer_txt, 0, 'C', '', 0, '', '', true, '', true, '', '', '', true);
        $this->SetFont('courier', '', 8);
        $w_page = isset($this->l['w_page']) ? $this->l['w_page'].' ' : '';
        if (empty($this->pagegroups)) {
            $pagenumtxt = $w_page.$this->getAliasNumPage().' / '.$this->getAliasNbPages();
        } else {
            $pagenumtxt = $w_page.$this->getPageNumGroupAlias().' / '.$this->getPageGroupAlias();
        }
        $this->SetY($cur_y);
        //Print page number
        if ($this->getRTL()) {
            $this->SetX($this->original_rMargin);
            $this->Cell(0, 0, $pagenumtxt, 'T', 0, 'L');
        } else {
            $this->SetX($this->original_lMargin);
            $this->Cell(0, 0, $this->getAliasRightShift().$pagenumtxt, 'T', 0, 'R');
        }
        
    }

    private function getHeight($array,$visible) {
        $height = 0;
        foreach ($array as $key => &$value) {
            if (in_array($key, $visible)){
                $w = $this->GetStringWidth(trim($value));
                if ($w >= 75) $w = 75;
                // print_r($w.'<br>');
                $cell_height = $this->getStringHeight($w,$value);
                if ($cell_height > $height) $height = $cell_height;
            } 
        }
        if ($height < 7) $height = 7;
        return $height;
    }

    private function printHeader($w, $header) {
        // Header
        $this->SetFont('', 'B');
        $i = 0;
        foreach ($header as $key => $value) {
            $this->Cell($w[$i], 7, $key, 'B', 0, 'L', 0);
            $i++;
        }
        $this->Ln();
        // Color and font restoration
        $this->SetFillColor(224, 235, 255);
        $this->SetTextColor(0);
        $this->SetFont('');
    }

    // Colored table
    public function ColoredTable($header,&$data,$w = array()) {
        if (empty($data)) {
            $this->SetTextColor(252,68,68);
            $this->writeHTML("Nessun dato da mostrate", true, 0, true, 0);
            $this->SetTextColor(0);
            return;
        }
        // Colors, line width and bold font
        $this->SetFillColor(232, 237, 255);
        $this->SetTextColor(0);
        $this->SetDrawColor(19, 95, 191);
        $this->SetLineWidth(0);

         // Dimension
        if (empty($w)){
            foreach ($header as $key => $value) {
                $array = array_column($data,$value);
                $array['title'] = $key;
                $width = $this->getWidth($array);
                if ($width >= 75) $width = 75;
                // if ($width >= 10 && $width <= 50) $width += 4;
                $w[] = $width;
            }
        }
        $this->SetFont('', '', 8);
        $this->printHeader($w,$header);
        // Data
        $fill = 0;
        foreach ($data as &$row) {
            $i = 0;
            $brake = false;
            $height = $this->getHeight($row,$header);
            if ($this->checkPageBreak($height,'',false)) {
                if ($height <= 8) {
                    $this->AddPage();
                    $this->printHeader($w,$header);
                } else {
                    $height = $this->PageBreakTrigger - $this->y - 4;
                    $brake = true;
                }
                
            }

            // print_r($row);
            foreach ($header as &$value) {
                if (empty($row[$value])) $row[$value] = '------';
                $width = $this->fwPt - $this->lMargin;
                // $this->MultiCell($w[$i], $height, $row[$value].' '.$w[$i], 0, 'L', $fill, 0, '', '', true, '', '', '', '', '', true);
                $this->MultiCell($w[$i], $height, $row[$value], 0, 'L', $fill, 0, '', '', true, '', '', '', '', '', true);
                $i++;
            }
            $fill=!$fill;
            $this->Ln();
            if ($brake) {
                $this->AddPage();
                $this->printHeader($w,$header);
            }
           
        }
        $this->SetFont('', '', 8);

        // $this->Cell(array_sum($w), 0, '', 'T');
    }

    public function drawLine($color = array(0,0,0)){
        $this->SetLineStyle(array('width' => 0.35 / $this->k, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => $color));
        $this->SetY((2.835 / $this->k) + max($imgy, $this->y));
        if ($this->rtl) {
            $this->SetX($this->original_rMargin);
        } else {
            $this->SetX($this->original_lMargin);
        }
        $this->Cell(($this->w - $this->original_lMargin - $this->original_rMargin), 0, '', 'T', 0, 'C');
    }

}
?>
